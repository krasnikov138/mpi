#include <iostream>
#include <fstream>
#include <math.h>

using namespace std;

class Solver
{
protected:
    double *u_prev;
    double *u;

    double a, b;
    double ksi;
    double h;
    int N;
    double T;

    double (*func)(double);

    void u_to_prev();
public:
    Solver(double (*f)(double), int N, double T,
           double ksi=1.0, double a=0.0, double b=1.0);
    friend ostream& operator<<(ostream &output, const Solver &obj);
    ~Solver();
};

Solver::Solver(double (*f)(double), int N, double T,
               double ksi, double a, double b)
{
    this->N = N; this->T = T;
    this->func = f;
    this->a = a; this->b = b;
    this->ksi = ksi;

    u = new double[N + 1];
    u_prev = new double[N + 1];

    u[0] = 0.0; u[N] = 0.0;
    u_prev[0] = 0.0; u_prev[N] = 0.0;

    h = (b - a)/N;

    for (int i = 1; i < N; i++)
        u[i] = func(h*i + a);
}

Solver::~Solver()
{
    delete [] u;
    delete [] u_prev;
}

void Solver::u_to_prev()
{
    for (int i = 1; i < N; i++)
        u_prev[i] = u[i];
}

ostream& operator<<(ostream &output, const Solver &obj)
{
    output << "x\tu(x)" << endl;

    for (int i = 0; i <= obj.N; i++)
        output << obj.h*i + obj.a << "\t" << obj.u[i] << endl;
    return output;
}

class LW: public Solver
{
protected:
    int M;
    double tau;

public:
    LW(double (*f)(double), int N, double T,
       double ksi=1.0, double a=0.0, double b=1.0) : Solver(f, N, T, ksi, a, b)
    {
        M = static_cast <int> (2*ksi*T/h);
        tau = T/M;
    }

    void solve();
};

void LW::solve()
{
    for (int i = 0; i <= M; i++)
    {
        u_to_prev();
        for (int j = 1; j < N; j++)
            u[j] = u_prev[j] - tau*ksi/h/2*(u_prev[j+1] - u_prev[j-1]) +
            pow(ksi*tau/h, 2)/2*(u_prev[j-1] - 2*u_prev[j] + u_prev[j+1]);
    }
}

class CIR: public Solver
{
protected:
    int M;
    double tau;

public:
    CIR(double (*f)(double), int N, double T,
        double ksi=1.0, double a=0.0, double b=1.0) : Solver(f, N, T, ksi, a, b)
    {
        M = static_cast <int> (2*ksi*T/h);
        tau = T/M;
    }

    void solve();
};

void CIR::solve()
{
    for (int i = 0; i <= M; i++)
    {
        u_to_prev();
        for (int j = 1; j < N; j++)
            u[j] = u_prev[j] - ksi*tau/h*(u_prev[j] - u_prev[j-1]);
    }
}

class SPS : public Solver
{
protected:
    int M;
    double tau;
    double gamma;

    double *a_coef;
    double *b_coef;
public:
    SPS(double (*f)(double), int N, int M, double T,
        double ksi=1.0, double a=0.0, double b=1.0) : Solver(f, N, T, ksi, a, b)
    {
         this->M = M;
         tau = T/M;
         gamma = ksi*tau/h;

         a_coef = new double[N];
         b_coef = new double[N];
         a_coef[0] = 0.0; b_coef[0] = 0.0;
    }

    void solve();

    ~SPS()
    {
        delete [] a_coef;
        delete [] b_coef;
    }
};

void SPS::solve()
{
    for (int i = 0; i <= M; i++)
    {
        u_to_prev();
        for (int j = 1; j < N; j++)
        {
            double d = u_prev[j-1] - gamma/4*(u_prev[j] - u_prev[j-2]);
            a_coef[j] = 1.0/(a_coef[j-1] - 4/gamma);
            b_coef[j] = -(b_coef[j-1] + 4*d/gamma)/(a_coef[j-1] - 4/gamma);
        }
        for (int j = N - 1; j > 0; j--)
            u[j] = a_coef[j+1]*u[j+1] + b_coef[j+1];
    }
}

double init_func(double x)
{
    if (x > -6 && x < 6)
        return 1.0;
    else
        return 0.0;
}

int main()
{
    CIR solver_cir(init_func, 10000, 5, 1.0, -20, 20);
    SPS solver_sps(init_func, 10000, 100, 5, 1.0, -20, 20);

    solver_cir.solve();
    solver_sps.solve();

    ofstream file_cir("result_cir.txt");
    ofstream file_sps("result_sps.txt");

    file_cir << solver_cir;
    file_sps << solver_sps;

    file_cir.close();
    file_sps.close();

    return 0;
}
