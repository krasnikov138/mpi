#include <iostream>
#include <stdio.h>
#include <mpi.h>
#include <math.h>
#include <fstream>


using namespace std;

double init(double x)
{
	if (x > -1 && x < 1)
		return 1.0;
	else
		return 0.0;
}

int main(int argc, char* argv[])
{
	int errCode;
	errCode = MPI_Init(&argc, &argv);
	if (errCode)
		return errCode;

	int numtasks, rank;
	double *u;
	double *u_prev;

	int N = 10000;
	int M;
	double a = -5;
	double b = 5;
	double T = 2.0;
	double ksi = 1.5;
	
	double h;

	double tau;
	double start; 
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	//printf("Number of tasks: %d my rank: %d\n", numtasks, rank);

	h = (b-a)/(N-1);
	M = (int) 2*T/h;
	tau = T/M;

	if (rank == 0)
	{
		start = MPI_Wtime();
		u = new double[N];
		u_prev = new double[N];

		u[0] = 0.0; u[N-1] = 0.0;
		for (int i = 1; i < N-1; i++)
			u[i] = init(i*h + a);

		for (int i = 0; i < M; i++)
		{		
			for (int j = 0; j < N; j++)
				u_prev[j] = u[j];

			for (int j = 1; j < N-1; j++)
				u[j] = u_prev[j] - tau*ksi/h*(u_prev[j] - u_prev[j-1]);
		}

		ofstream file("result_cir.txt");
		file << "x\tu(x)" << endl;
		for (int i = 0; i < N; i++)
			file << i*h + a << "\t" << u[i] << endl;
		file.close();

		cout << "Time: " << MPI_Wtime() - start << endl;
	}

	delete [] u;
	delete [] u_prev;
	MPI_Finalize();
	return 0;
}
