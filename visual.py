import pandas as pd
from matplotlib import pyplot as plt

#load data
data_lw = pd.read_table("result_lw.txt", header = 0)
data_cir = pd.read_table("result_cir.txt", header = 0)
init = pd.read_table("init.txt", header = 0)

datas = [data_lw, data_cir]
names = ["LW", "CIR"]

#plotting data using matplotlib
fig, axarr = plt.subplots(2, 1, sharex=True, sharey=True)
fig.set_size_inches(10, 8, forward=True)
for ax, data, name in zip(axarr, datas, names):
	#plot init line and transfered line
	ax.plot(data['x'], data['u(x)'], 'b', label='sol')
	ax.plot(init['x'], init['u(x)'], 'g--', label='init')

	#add title, labels and legend
	ax.set_title(name)
	ax.set_xlabel(data.columns[0])
	ax.set_ylabel(data.columns[1])
	ax.legend(loc='upper left')

plt.show()
