#include <iostream>
#include <mpi.h>
#include <math.h>
#include <fstream>
#include <cstdlib>

using namespace std;

//basic class
//to calculate arbitrary scheme this class should be inherited
class Solver
{
protected:
	//the current and previous layer of the scheme
	double *u_prev;
	double *u;

	//some constant of the problem
	//their description can be found in README
	double a, b;
	double ksi;
	double h;
	int N;
	int n;
	double T;

	//the function used to initialize the first layer
	double (*func)(double);

	//function used to copy current layer to previous one
	void u_to_prev();
	//function used to collect data from all subprocesses
	//in the first one
	void collect_data();

	//for management of initialization and
	//finalization of mpi
	static int num_obj;
	static int numtasks;
	static int rank;
public:
	Solver(double (*f)(double), int N, double T,
		   double ksi=1.0, double a=0.0, double b=1.0);

	friend ostream& operator<<(ostream &output, const Solver &obj);

	//for initializaton of CLASS
	//(used before creation of class instances
	static void Init(int &argc, char** &argv);
	~Solver();
};

//init of static variables
int Solver::num_obj = -1;
int Solver::rank = 0;
int Solver::numtasks = 0;

//constructor
Solver::Solver(double (*f)(double), int N, double T,
		   double ksi, double a, double b)
{
	//check if the class was initialized
	if (num_obj < 0)
	{
		cout << "Not initialized MPI!" << endl;
		exit(-1);
	}

	//register the creation of a class instance
	num_obj++;

	//save constants of the task
	this->a = a; this->b = b;
	this->N = ((N + numtasks - 1)/numtasks)*numtasks;
	this->n = this->N/numtasks;
	this->h = (b - a)/(this->N - 1);
	this->ksi = ksi;
	this->func = f;
	this->T = T;

	//allocate memory in different subprocesses
	if (rank == 0)
		u = new double[this->N];
	else
		u = new double[n];
	u_prev = new double[n];

	//calculation of the first layer
	for (int i = 0; i < n; i++)
		u[i] = func((i + n * rank) * h + a);
}

//initialization of MPI system
void Solver::Init(int &argc, char** &argv)
{
	//check if the MPI system has already been initialized
	if (num_obj == -1)
	{
		int errCode;
		errCode = MPI_Init(&argc, &argv);
		if (errCode)
			exit(errCode);

		//get number of subprocesses and their ids
		MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);

		//now class instances can be created
		num_obj = 0;
	}
}

//copy current layer to prev
void Solver::u_to_prev()
{
	for (int j = 0; j < n; j++)
		u_prev[j] = u[j];
}

//gather solution in the first subprocess
void Solver::collect_data()
{
	if (rank)
		//subprocesses with rank > 0 are slaves
		//subproc with rank == 0 is the master
		//slaves send what they solved to the master
		MPI_Ssend(u, n, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	else
		//the master received all data from slaves
		for (int i = 1; i < numtasks; i++)
			MPI_Recv(u + i * n, n, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

//print the result from the stream
//should be used after collect_data
ostream& operator<<(ostream &output, const Solver &obj)
{
	//the result is printed in format "x u(x)\n"
	if (obj.rank == 0)
	{
		output << "x\tu(x)" << endl;

		for (int i = 0; i < obj.N; i++)
			output << obj.a + obj.h*i << "\t" << obj.u[i] << endl;
	}
	return output;
}

//destructor
Solver::~Solver()
{
	//decrease the number of class instances
	num_obj--;
	//free allocated memory
	delete [] u;
	delete [] u_prev;

	//if there are no any instances finalize mpi
	if (num_obj == 0)
	{
		num_obj = -1;
		MPI_Finalize();
	}
}

//the solver class for solution the transfer equation
//using Lax-Wendroff sceme
class LW : public Solver
{
protected:
	//constants connected with time
	int M;
	double tau;

public:
	LW(double (*f)(double), int N, double T,
	   double ksi=1.0, double a=0.0, double b=1.0) : Solver(f, N, T, ksi, a, b)
	{
		M = static_cast <int> (2*ksi*T/h);
		tau = T/M;
	}
	//main function which starts the loop for solution
	void solve();
};

//the implenetation of the main function
void LW::solve()
{
	//time loop
	for (int i = 0; i < M; i++)
	{
		//save current u in u_prev
		u_to_prev();

		//neighbors processes exchange boundary values with each other
		if (rank == 0)
		{
			double r;
			MPI_Ssend(&u[n-1], 1, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD);
			MPI_Recv(&r, 1, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			//calculate next boundary value
			u[n-1] = u_prev[n-1] - ksi*tau/h/2*(r - u_prev[n-2]) +
				pow(ksi*tau/h,2)/2*(u_prev[n-2] - 2*u_prev[n-1] + r);
			u[0] = 0.0;
		}
		else if (rank != numtasks - 1)
		{
			double l, r;
			MPI_Recv(&l, 1, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			MPI_Ssend(&u[0], 1, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD);
			MPI_Ssend(&u[n-1], 1, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD);
			MPI_Recv(&r, 1, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			//calculate next boundary values
			u[n-1] = u_prev[n-1] - ksi*tau/h/2*(r - u_prev[n-2]) +
				pow(ksi*tau/h,2)/2*(u_prev[n-2] - 2*u_prev[n-1] + r);
			u[0] = u_prev[0] - ksi*tau/h/2*(u[1] - l) +
				pow(ksi*tau/h, 2)/2*(l - 2*u[0] + u[1]);
		}
		else
		{
			double l;
			MPI_Recv(&l, 1, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			MPI_Ssend(&u[0], 1, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD);

			//calculate next boundary values
			u[0] = u_prev[0] - ksi*tau/h/2*(u[1] - l) +
				pow(ksi*tau/h, 2)/2*(l - 2*u[0] + u[1]);
			u[n-1] = 0.0;
		}

		//each process calculute u(x) in the next layer
		for (int j = 1; j < n-1; j++)
			u[j] = u_prev[j] - ksi*tau/h/2*(u_prev[j+1] - u_prev[j-1]) +
				pow(ksi*tau/h,2)/2*(u_prev[j-1] - 2*u_prev[j] + u_prev[j+1]);
	}

	//gather all data in the first process
	collect_data();
}

double init(double x)
{
	if (x > -1 && x < 1)
		return 1.0;
	else
		return 0.0;
}

//example of LW class usage
int main(int argc, char** argv)
{
	//MPI init
	LW::Init(argc, argv);

	//set the task parameters (N, T, ksi, a, b)
	LW lw(init, 10000, 1.5, 2.0, -5.0, 5.0);
	//solve the problem
	lw.solve();

	//write the result in result.txt file
	ofstream file("result_lw.txt");
	file << lw;
	file.close();

	return 0;
}
